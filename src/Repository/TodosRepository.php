<?php

namespace App\Repository;

use App\Entity\Todos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Todos>
 *
 * @method Todos|null find($id, $lockMode = null, $lockVersion = null)
 * @method Todos|null findOneBy(array $criteria, array $orderBy = null)
 * @method Todos[]    findAll()
 * @method Todos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TodosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Todos::class);
    }

    public function add(Todos $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Todos $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    
    public function delete(int $id): void
    {
        $entity = $this->find($id);
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }
    
    
   /**
    * @return Todos[] Returns an array of Todos objects
    */
   public function findByParams(array $params, $sortBy): array
   {
        $query = $this->createQueryBuilder('t');
        
//         if ($params['author']) {
//             $query = $query->where('t.author = :author')->setParameter('author', $params['author']);
//         }
        
//         if ($params['status'] ?? false && in_array(strtoupper($params['status']), ['DONE', 'TODOS'])) {
//             $query = $query->andWhere('t.status = :status')->setParameter('status', strtoupper($params['status']));
//         }
        
//         if ($params['title'] ?? false) {
//             $title = $params['title'];
//             $query = $query->andWhere('t.title LIKE :title')
//                 ->setParameter('title', '%' . trim($title) . '%');
//         }
        
//         if (isset($params['priority'])) {
//             if (!is_array($params['priority'])) {
//                 $priorities = explode(',', $params['priority']);
//                 switch (sizeof($priorities)) {
//                     case 2:
//                         $query = $query->andWhere('t.priority >= :from AND t.priority <= :to')
//                             ->setParameter('from', intval($priorities[0]))
//                             ->setParameter('to', intval($priorities[1]));
//                         break;
//                     case 1:
//                         $query = $query->andWhere('t.priority = :priority')
//                             ->setParameter('priority', intval($params['priority']));
//                         break;
//                 }
//             }
//         }
        
//         if (!in_array($sortBy, ['created', 'priority'])) $sortBy = 'created';
        
//         return $query->orderBy('t.' . $sortBy, 'ASC')->getQuery()->getResult();
   }
}
