<?php 

namespace App\Service;

use App\Repository\TodosRepository;
use Doctrine\ORM\QueryBuilder;
use App\Entity\Todos;

class TodosFinder {

    private QueryBuilder $builder;
    
    public function __construct(TodosRepository $repository)
    {
        $this->builder = $repository->createQueryBuilder('t');
    }
    
    public function authorFilter(int $filter)
    {
        if (empty($filter)) return $this;
        
        $this->builder = $this->builder->where('t.author = :author')->setParameter('author', $filter);
        
        return $this;
    }
    
    public function statusFilter(?string $filter)
    {
        if (empty($filter) || in_array(strtoupper($filter), [Todos::STATUS_DONE, Todos::STATUS_TODOS])) return $this;
        
        $this->builder = $this->builder->where('t.status = :status')->setParameter('status', $filter);
        
        return $this;
    }
    
    public function titleFilter(?string $filter)
    {
        if (empty($filter)) return $this;
        
        $this->builder = $this->builder->andWhere('t.title LIKE :title')->setParameter('title', '%' . trim($filter) . '%');
        
        return $this;
    }
    
    public function priorityFilter(?string $filter)
    {
        if (empty($filter)) return $this;
        
        $priorities = explode(',', $filter);
        switch (sizeof($priorities)) {
            case 2:
                $this->builder = $this->builder->andWhere('t.priority >= :from AND t.priority <= :to')
                    ->setParameter('from', intval($priorities[0]))
                    ->setParameter('to', intval($priorities[1]));
                break;
            case 1:
                $this->builder = $this->builder->andWhere('t.priority = :priority')
                    ->setParameter('priority', intval($priorities[0]));
                break;
        }
        
        return $this;
    }

    public function orderBy(?string $sortBy)
    {
        if (!in_array($sortBy, ['created', 'priority'])) $sortBy = 'created';
        
        $this->builder = $this->builder->orderBy('t.' . $sortBy, 'ASC');
        
        return $this;
    }
    
    public function getResult()
    {
        return $this->builder->getQuery()->getResult();
    }
}
