<?php

namespace App\Entity;

use App\Repository\TodosRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TodosRepository::class)]
class Todos
{
    const STATUS_DONE = 'DONE';

    const STATUS_TODOS = 'TODOS';
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 127)]
    private $title;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\Column(type: 'string', length: 63, nullable: true)]
    private $status;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $priority;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'todos')]
    private $parent;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class, cascade: ["remove"])]
    private $todos;
    
    #[ORM\ManyToOne(targetEntity: Author::class)]
    private $author;
    
    #[ORM\Column(type: 'datetime', nullable: true)]
    private $created;
    
    
    public function __construct()
    {
        $this->todos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(?int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getTodos(): Collection
    {
        return $this->todos;
    }

    public function addTodo(self $todo): self
    {
        if (!$this->todos->contains($todo)) {
            $this->todos[] = $todo;
            $todo->setParent($this);
        }

        return $this;
    }

    public function removeTodo(self $todo): self
    {
        if ($this->todos->removeElement($todo)) {
            // set the owning side to null (unless already changed)
            if ($todo->getParent() === $this) {
                $todo->setParent(null);
            }
        }

        return $this;
    }
    
    public function getAuthor()
    {
        return $this->author;
    }
    
    public function setAuthor(?Author $author): self
    {
        $this->author = $author;
        
        return $this;
    }

    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }
    
    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;
        
        return $this;
    }
}
