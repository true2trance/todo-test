<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\AuthorRepository;

class LoginController extends AbstractController
{
    #[Route('/api/login', name: 'app_login')]
    public function index(Request $request, JWTTokenManagerInterface $JWTManager, AuthorRepository $repository): JsonResponse
    {   
        try {
            $author = $repository->findOneBy(json_decode($request->getContent(), true));
            
            return new JsonResponse(['token' => $JWTManager->create($author)]);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()]);
        }
    }
}
