<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\TodosRepository;
use App\Entity\Todos;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Service\TodosFinder;

#[Route("/api/todos")]
class TodosController extends AbstractController
{
    protected SerializerInterface $serializer;
    protected TokenStorageInterface $storage;
    protected array $headers;
    
    public function __construct(TodosRepository $repository, SerializerInterface $serializer, TokenStorageInterface $storage)
    {
        $this->repository = $repository;
        $this->type = Todos::class;
        $this->serializer = $serializer;
        $this->storage = $storage;
        $this->headers = ['Content-Type' => 'application/json'];
    }
    
    #[Route('/', name: 'app_todos', methods: ['GET'])]
    public function index(Request $request, TodosFinder $finder): Response
    {
        $data = $request->query->all();
        
        $todos = $finder
            ->authorFilter($this->storage->getToken()->getUser()->getId() ?? null)
            ->statusFilter($data['status'] ?? null)
            ->titleFilter($data['title'] ?? null)
            ->priorityFilter($data['priority'] ?? null)
            ->orderBy($data['sort_by'] ?? null)
            ->getResult();
           
        return new Response(
            $this->serializer->serialize($todos,'json'), 
            Response::HTTP_OK, 
            $this->headers
        );
    }
    
    #[Route('/', name: 'app_todos_create', methods: ['POST'])]
    public function create(Request $request): Response
    {
        $todo = $this->serializer->deserialize($request->getContent(), $this->type, 'json');
        $this->repository->add($todo, true);
        
        return new Response(
            $this->serializer->serialize($todo, 'json'),
            Response::HTTP_CREATED, 
            $this->headers
        );
    }
    
    #[Route('/{id}', name: 'app_todos_view', methods: ['GET'])]
    public function view(int $id): Response
    {
        return new Response(
            $this->serializer->serialize(
                $this->repository->find($id), 
                'json'
            ),
            Response::HTTP_OK, 
            $this->headers
        );
    }
    
    #[Route('/', name: 'app_todos_update', methods: ['PUT'])]
    public function update(Request $request)
    {
        $todo = $this->serializer->deserialize($request->getContent(), $this->type, 'json');
        $this->repository->add($todo, true);
        
        return new Response(
            $this->serializer->serialize($todo, 'json'), 
            Response::HTTP_ACCEPTED,
            $this->headers
        );
}
    
    
    #[Route('/{id}', name: 'app_todos_delete', methods: ['DELETE'])]
    public function delete(int $id)
    {
        $this->repository->delete($id);
        
        return new JsonResponse(['result' => 'OK']);
    }
}
