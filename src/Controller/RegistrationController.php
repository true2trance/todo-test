<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerInterface;
use App\Entity\Author;
use App\Repository\AuthorRepository;

class RegistrationController extends AbstractController
{
    #[Route('/api/registration', name: 'app_registration')]
    public function index(Request $request, SerializerInterface $serializer, AuthorRepository $repository): JsonResponse
    {
        try {
            $author = $serializer->deserialize($request->getContent(), Author::class, 'json');
            $repository->add($author, true);
            
            return $this->json([
                'result' => 'OK'
            ]);
        } catch (\Exception $e) {
            return $this->json([
                'error' => $e->getMessage()
            ]);
        }
    }
}
