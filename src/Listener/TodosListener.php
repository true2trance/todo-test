<?php

namespace App\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use App\Entity\Todos;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class TodosListener
{
    private TokenStorageInterface $storage;
    
    public function __construct(TokenStorageInterface $storage)
    {
        $this->storage = $storage;
    }
    
    /**
     * @param LifecycleEventArgs $args
     * @return bool
     * @throws \Exception
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Todos) return null;

        $user = $this->storage->getToken() != null ? $this->storage->getToken()->getUser() : null;
        
        if (!$user || $user->getId() != $entity->getAuthor()->getId()) {
            throw new \Exception('It is not your task!');
        }
        
        if ($entity->getStatus() === 'DONE') {
            throw new \Exception('Can not change Status! Task #' . $entity->getId() . ' is done!');
        }
    }
    
    /**
     * @param LifecycleEventArgs $args
     * @return bool
     * @throws \Exception
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        
        if (!$entity instanceof Todos) return null;
        
        $user = $this->storage->getToken() != null ? $this->storage->getToken()->getUser() : null;
        
        if (!$user || !$entity->getAuthor() || $user->getId() != $entity->getAuthor()->getId()) {
            throw new \Exception('It is not your task!');
        }
        
        if ($entity->getStatus() === 'DONE') {
            throw new \Exception('Can not delete! Task #' . $entity->getId() . ' is done!');
        }
    }
    
    /**
     * @param LifecycleEventArgs $args
     * @return null
     * @throws \Exception
     */
    public function prePersist(LifecycleEventArgs  $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Todos) return null;

        $author = $this->storage->getToken()->getUser();
        
        $entity->setAuthor($author);
        
        $entity->setCreated(new \DateTime('now'));
    }
}
