<?php

namespace App\Listener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $message = json_encode(['error' => sprintf(
            'Error: %s (code: %s)',
            $exception->getMessage(),
            $exception->getCode()
            )]);

        $response = new Response();
        $response->setContent($message);
        $response->headers->add(['Content-Type' => 'application/json']);

        $event->setResponse($response);
    }
}
