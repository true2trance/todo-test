INSTALLATION

1. copy .env.example to .env

2. composer install

3. php bin/console d:s:u -f

4. php bin/console doctrine:fixtures:load

5. php -S 127.0.0.1:3002


ENDPOINTS EXAMPLES

1. GET http://127.0.0.1:3002/api/todos/?title=T&priority=1,5&status=todos&sort_by=priority

Response: 
[
    {
        "id": 8,
        "title": "T8888",
        "description": "d5",
        "status": "TODOS",
        "priority": 5,
        "parent": {
            "id": 7,
            "title": "T777",
            "description": "d5",
            "status": "TODOS",
            "priority": 3,
            "todos": [],
            "author": {
                "id": 1,
                "username": "Author1"
            },
            "created": "2022-02-11T00:00:00+00:00"
        },
        "todos": [
            {
                "id": 9,
                "title": "T9",
                "description": "d5",
                "status": "TODOS",
                "priority": 1,
                "todos": [],
                "author": {
                    "id": 1,
                    "username": "Author1",
                    "roles": [],
                    "password": "pass"
                },
                "created": "2022-05-29T15:55:32+00:00"
            }
        ],
        
        ...
]        
        


2. POST http://127.0.0.1:3002/api/todos

Body (Request):

{
	"title": "T10",
	"description": "d5",
	"status": "TODOS",
	"priority": 1,
	"parent": {
		"id": 3
	}
}

Response:

{
    "id": 10,
    "title": "T10",
    "description": "d5",
    "status": "TODOS",
    "priority": 1,
    "author": {
        "id": 1,
        "username": "Author1"
    },
    "created": "2022-05-29T16:06:41+00:00"
}



3. PUT http://127.0.0.1:3002/api/todos

Body (Request):

{
	"id": 9,
	"title": "T10",
	"description": "d5",
	"status": "TODOS",
	"priority": 1,
	"parent": {
		"id": 3
	}
}

Response:

{
    "id": 10,
    "title": "T10",
    "description": "d5",
    "status": "TODOS",
    "priority": 1,
    "author": {
        "id": 1,
        "username": "Author1"
    },
    "created": "2022-05-29T16:06:41+00:00"
}


5. POST http://127.0.0.1:3002/api/registration

Body (Request):

{
	"username": "Author1",
	"password": "pass"
}

Response:

{
    "result": "OK"
}


6. POST http://127.0.0.1:3002/api/registration

Body (Request):

{
	"username": "Author1",
	"password": "pass"
}

Response:

{
    "token": "JWT_TOKEN"
}


7. GET http://127.0.0.1:3002/api/todos/8

Response: 

{
    "id": 8,
    "title": "T8888",
    "description": "d5",
    "status": "TODOS",
    "priority": 5,
    "parent": {
        "id": 7,
        "title": "T777",
        "description": "d5",
        "status": "TODOS",
        "priority": 3,
        "todos": [],
        "author": {
            "id": 1,
            "username": "Author1"
        },
        "created": "2022-02-11T00:00:00+00:00"
    },
    "todos": [],
    "created": "2022-02-11T00:00:00+00:00"
}        


8. DELETE http://127.0.0.1:3002/api/todos/7

Response:

{
    "result": "OK"
}
